const getFormattedUserInfo = data => ({
  username: data.username,
  email: data.email,
  usernameWithRole: data.username_with_role,
  firstName: data.first_name,
  lastName: data.last_name,
  patronymic: data.patronymic,
  role: data.role,
});

export const state = () => ({
  username: "",
  email: "",
  usernameWithRole: "",
  firstName: "",
  lastName: "",
  patronymic: "",
  role: "",
});

export const getters = {
  isLoggedIn: state => state.username.length > 0,
  isGuest: state => state.username.length === 0,
  isStudent: state => state.role === "Student",
  isTeacher: state => state.role === "Teacher",
  isAdministrator: state => state.role === "Administrator",
};

export const mutations = {
  fillData(state, { username, email, usernameWithRole, firstName, lastName, patronymic, role }) {
    state.username = username;
    state.email = email;
    state.usernameWithRole = usernameWithRole;
    state.firstName = firstName;
    state.lastName = lastName;
    state.patronymic = patronymic;
    state.role = role;
  },

  clearData(state) {
    state.username = state.email = state.usernameWithRole = state.firstName = state.lastName = state.patronymic = state.role =
      "";
  },
};

export const actions = {
  async logIn({ commit }, payload) {
    const {
      data: { user },
    } = await this.$axios.$post("api/sessions", { auth: payload }).catch(err => {
      commit("clearData");
    });

    commit("fillData", getFormattedUserInfo(user));
  },

  async getUserData({ commit }) {
    const { user } = await this.$axios.$get("api/sessions");

    commit("fillData", getFormattedUserInfo(user));
  },

  async logOut({ commit }) {
    await this.$axios.$delete("api/sessions");

    commit("clearData");
  },
};
